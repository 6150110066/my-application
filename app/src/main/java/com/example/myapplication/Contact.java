package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Contact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
    }

    public void gotoFacebook(View view) {
        Intent fb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/beebahsk/"));
        startActivity(fb);
    }

    public void gotoInstargram(View view) {
        Intent ig = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/beebahsk/?hl=th"));
        startActivity(ig);
    }

    public void gotoLine(View view) {
        Intent line = new Intent(Intent.ACTION_VIEW, Uri.parse("https://line.me/ti/p/eHzv1CpWaU"));
        startActivity(line);
    }

    public void gotoHome(View view) {
        Intent home = new Intent(this,MainActivity.class);
        startActivity(home);
    }
}