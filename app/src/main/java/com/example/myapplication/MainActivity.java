package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gotoProfile(View view) {
        Intent i = new Intent(this,Profile.class);
        startActivity(i);
    }

    public void gotoContact(View view) {
        Intent i = new Intent(this,Contact.class);
        startActivity(i);
    }

}