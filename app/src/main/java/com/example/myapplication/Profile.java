package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }

    public void gotoHome(View view) {
        Intent home = new Intent(this,MainActivity.class);
        startActivity(home);
    }

    public void gotoContact(View view) {
        Intent contact = new Intent(this,Contact.class);
        startActivity(contact);
    }

    public void exit(View view) {
        Intent exit = new Intent();
        exit.putExtra("returnkey", "cancle");
        setResult(RESULT_CANCELED, exit);
        super.finish();
    }
}